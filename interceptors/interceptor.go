package interceptors

import (
	"bitbucket.org/desperation13/astral/pkg/auth"
	"bitbucket.org/desperation13/astral/users"
	"bitbucket.org/desperation13/astral/users/google_info"
	"bitbucket.org/desperation13/astral/users/vk_info"
	"fmt"
	"net/http"
)

type userRepo interface {
	Save(u *users.User) error
	GetByID(id uint) (*users.User, error)
	GetByEmail(email string) (*users.User, error)
	GetByServiceID(serviceID string, serviceName auth.Type) (*users.User, error)
}

type googleAuthRepo interface {
	Save(u *google_info.GoogleAuthInfo) error
	GetInfoByGoogleServiceID(serviceID string) (*google_info.GoogleAuthInfo, error)
	GetInfoByHashedToken(hT string) (*google_info.GoogleAuthInfo, error)
}

type vkAuthRepo interface {
	Save(u *vk_info.VkAuthInfo) error
	GetInfoByVkServiceID(serviceID string) (*vk_info.VkAuthInfo, error)
	GetInfoByHashedToken(hT string) (*vk_info.VkAuthInfo, error)
}

type Data struct {
	User   *users.User
	Cookie *http.Cookie
}

const (
	GOOGLE_ENC_TOKEN = "gstate"
	VK_ENC_TOKEN     = "vkstate"
)

type CommandInterceptor struct {
	google Interceptor
	vk     Interceptor
}

func NewCommandInterceptor(google, vk Interceptor) *CommandInterceptor {
	return &CommandInterceptor{
		google: google,
		vk:     vk,
	}
}

func (i *CommandInterceptor) Do(r *http.Request) (Data, error) {
	gCookie, err := r.Cookie(GOOGLE_ENC_TOKEN)
	if err != nil {
		vkCookie, err := r.Cookie(VK_ENC_TOKEN)
		if err != nil {
			return Data{}, fmt.Errorf("no cookie was found")
		}
		return i.vk.Do(vkCookie)
	}
	return i.google.Do(gCookie)
}

type Interceptor interface {
	Do(cookie *http.Cookie) (Data, error)
}
