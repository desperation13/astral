package interceptors

import (
	"bitbucket.org/desperation13/astral/pkg/auth"
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"
)

type GoogleInterceptor struct {
	Secret32bit       []byte
	GoogleAuthRepo    googleAuthRepo
	GoogleOAuthConfig *auth.GoogleConfig
	UserRepo          userRepo
}

func (i *GoogleInterceptor) Do(cookie *http.Cookie) (Data, error) {
	if cookie == nil {
		return Data{}, http.ErrNoCookie
	}
	b64T, err := base64.URLEncoding.DecodeString(cookie.Value)
	if err != nil {
		return Data{}, err
	}
	token, err := hashlib.Decrypt(b64T, i.Secret32bit)
	if err != nil {
		return Data{}, err
	}
	resp := Data{}
	data, err := i.GoogleOAuthConfig.GetData(string(token))
	if err != nil {
		// get user by hashed access token
		info, err := i.GoogleAuthRepo.GetInfoByHashedToken(hashlib.HashAndSalt(token, i.Secret32bit))
		if err != nil {
			return Data{}, err
		}
		if info == nil {
			return Data{}, fmt.Errorf("not found")
		}
		user, err := i.UserRepo.GetByID(info.UserID)
		if err != nil {
			return Data{}, err
		}
		token, err := i.GoogleOAuthConfig.RefreshAccessToken(info.RefreshToken)
		if err != nil {
			return Data{}, err
		}
		hT := hashlib.HashAndSalt(token, i.Secret32bit)
		info.HashedToken = hT
		if err := i.GoogleAuthRepo.Save(info); err != nil {
			return Data{}, err
		}

		encT, err := hashlib.EncryptAndEncode(string(token), i.Secret32bit)
		if err != nil {
			return Data{}, err
		}
		resp.Cookie = &http.Cookie{
			Name:    GOOGLE_ENC_TOKEN,
			Value:   encT,
			Expires: time.Now().Add(time.Hour * 24 * 30 * 6),
			Path:    "/api",
		}
		resp.User = user
	} else {
		user, err := i.UserRepo.GetByServiceID(data.GoogleID, auth.GOOGLE)
		if err != nil {
			return Data{}, err
		}
		resp.User = user
	}
	return resp, nil
}
