package interceptors

import (
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"encoding/base64"
	"github.com/pkg/errors"
	"net/http"
)

type VkInterceptor struct {
	Secret32bit []byte
	UserRepo    userRepo
	VkAuthRepo  vkAuthRepo
}

func (v *VkInterceptor) Do(cookie *http.Cookie) (Data, error) {
	b64T, err := base64.URLEncoding.DecodeString(cookie.Value)
	if err != nil {
		return Data{}, err
	}
	token, err := hashlib.Decrypt(b64T, v.Secret32bit)
	if err != nil {
		return Data{}, err
	}
	info, err := v.VkAuthRepo.GetInfoByHashedToken(hashlib.HashAndSalt(token, v.Secret32bit))
	if err != nil || info == nil {
		return Data{}, errors.Wrap(err, "vk_auth_info not found or error occurred")
	}
	user, err := v.UserRepo.GetByID(info.UserID)
	if err != nil || user == nil {
		return Data{}, errors.Wrap(err, "user not found or error occurred")
	}
	return Data{
		User: user,
	}, nil
}
