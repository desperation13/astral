package buffs

import (
	"github.com/jinzhu/gorm"
	"time"
)

type BuffEntity struct {
	gorm.Model

	Name                string
	ExpirationTimeHours uint
	ImgURL              string
	Text                string

	ChakraID uint `sql:"type:integer REFERENCES chakras(id);not null"`
}

type Buff struct {
	gorm.Model

	EndsAt time.Time

	BuffEntityID uint `sql:"type:integer REFERENCES buff_entities(id);not null"`
	UserID       uint `sql:"type:integer REFERENCES users(id);not null"`
}
