package buffs

import "github.com/jinzhu/gorm"

type BuffEntityRepo struct {
	db *gorm.DB
}

func NewBuffEntityRepo(db *gorm.DB) *BuffEntityRepo {
	return &BuffEntityRepo{db: db}
}

func (r *BuffEntityRepo) GetByName(name string) (*BuffEntity, error) {
	var b BuffEntity
	err := r.db.Take(&b, "name = ?", name).Error
	return &b, err
}
