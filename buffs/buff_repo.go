package buffs

import "github.com/jinzhu/gorm"

type BuffRepo struct {
	db *gorm.DB
}

func NewBuffRepo(db *gorm.DB) *BuffRepo {
	return &BuffRepo{db: db}
}

func (r *BuffRepo) Save(b *Buff) error {
	return r.db.Save(b).Error
}
