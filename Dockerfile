FROM    golang:1.13.3-alpine3.10 AS builder
ARG     ENABLE_GO_MODULES
ENV     GO111MODULE=${ENABLE_GO_MODULES}

RUN     apk update && apk add bash ca-certificates git gcc g++ libc-dev binutils file
WORKDIR /go/src/bitbucket.org/desperation13/astral
COPY    go.mod .
COPY    go.sum .
RUN     go mod download
COPY    . .
RUN     ls -la .
RUN     go build -o /app -x -v cmd/api/main.go
RUN     ls -la /app

FROM alpine:3.10 AS production
RUN  apk update && apk add ca-certificates libc6-compat && rm -rf /var/cache/apk/*
COPY --from=builder /app ./
RUN  ls -la ./
