package main

import (
	"bitbucket.org/desperation13/astral/api"
	"bitbucket.org/desperation13/astral/app"
	"bitbucket.org/desperation13/astral/conf"
	"github.com/sirupsen/logrus"
	"net/http"
)

func main() {
	config, err := conf.ReadConfig()
	if err != nil {
		logrus.Fatal(err)
	}
	serv := app.Init(config)
	h := api.NewHandler(serv)
	logrus.Fatal(http.ListenAndServe(config.Port, h))
}
