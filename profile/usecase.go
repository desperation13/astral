package profile

import (
	"bitbucket.org/desperation13/astral/buffs"
	"bitbucket.org/desperation13/astral/chakras"
	"github.com/jinzhu/gorm"
)

type UseCase struct {
	db *gorm.DB
}

type BuffInfo struct {
	buffs.Buff
	buffs.BuffEntity
	chakras.Chakra
}

func NewUseCase(db *gorm.DB) *UseCase {
	return &UseCase{db: db}
}

func (u *UseCase) GetUserBuffInfos(userID uint) ([]BuffInfo, error) {
	var res []BuffInfo

	beJq := "inner join buff_entities as be on be.id = b.buff_entity_id"
	chJq := " inner join chakras as c on c.id = be.chakra_id"
	err := u.db.Table("buffs as b").Joins(beJq+chJq).Select("*").Where("user_id = ?", userID).Scan(&res).Error
	return res, err
}
