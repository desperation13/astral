package app

import (
	"bitbucket.org/desperation13/astral/api"
	"bitbucket.org/desperation13/astral/buffs"
	"bitbucket.org/desperation13/astral/chakras"
	"bitbucket.org/desperation13/astral/conf"
	"bitbucket.org/desperation13/astral/interceptors"
	"bitbucket.org/desperation13/astral/pkg/auth"
	"bitbucket.org/desperation13/astral/profile"
	"bitbucket.org/desperation13/astral/users"
	"bitbucket.org/desperation13/astral/users/google_info"
	"bitbucket.org/desperation13/astral/users/jwt_info"
	"bitbucket.org/desperation13/astral/users/password_changes"
	"bitbucket.org/desperation13/astral/users/vk_info"
	"github.com/davecgh/go-spew/spew"
	"github.com/go-chi/jwtauth"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"golang.org/x/oauth2/vk"
	"net/http"
	"strconv"
	"time"
)

func Init(config *conf.Config) *api.Server {
	spew.Dump(config)
	db, err := gorm.Open("postgres", config.Postgres)
	if err != nil {
		logrus.Fatal(err)
	}
	db.AutoMigrate(
		&users.User{},
		&google_info.GoogleAuthInfo{},
		&vk_info.VkAuthInfo{},
		&jwt_info.JWTAuthInfo{},
		&password_changes.PasswordChange{},
		&chakras.Chakra{},
		&buffs.BuffEntity{},
		&buffs.Buff{},
	)

	userRepo := users.NewRepo(db)
	gAuthRepo := google_info.NewGoogleRepo(db)
	vkAuthRepo := vk_info.NewVkRepo(db)
	simpleAuthRepo := jwt_info.NewSimpleAuthRepo(db)
	passwordChangeRepo := password_changes.NewRepo(db)
	buffEntityRepo := buffs.NewBuffEntityRepo(db)
	buffRepo := buffs.NewBuffRepo(db)
	chakraRepo := chakras.New(db)
	profileUsecase := profile.NewUseCase(db)

	exp, err := strconv.Atoi(config.JWTExpiry)
	if err != nil {
		logrus.Fatal(err)
	}

	gconfig := &oauth2.Config{
		RedirectURL:  config.GoogleOAuth.RedirectURL,
		ClientID:     config.GoogleOAuth.ClientID,
		ClientSecret: config.GoogleOAuth.ClientSecret,
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"},
		Endpoint:     google.Endpoint,
	}
	vkconfig := &oauth2.Config{
		ClientID:     config.VKOAuth.ClientID,
		ClientSecret: config.VKOAuth.ClientSecret,
		Endpoint:     vk.Endpoint,
		RedirectURL:  config.VKOAuth.RedirectURL,
		Scopes:       []string{"email", "offline"},
	}
	gC := &auth.GoogleConfig{
		Config:      gconfig,
		StateName:   "goauthstate",
		ResourceURL: "https://www.googleapis.com/oauth2/v2/userinfo?access_token=",
		RefreshURL:  "https://oauth2.googleapis.com/token",
		Client: &http.Client{
			Timeout: 60 * time.Second,
		},
	}
	vC := &auth.VkConfig{
		Config:      vkconfig,
		StateName:   "vkauthstate",
		ResourceURL: "https://oauth.vk.com/access_token",
	}

	googleInterceptor := &interceptors.GoogleInterceptor{
		Secret32bit:       []byte(config.Secret32Bit),
		UserRepo:          userRepo,
		GoogleAuthRepo:    gAuthRepo,
		GoogleOAuthConfig: gC,
	}
	vkInterceptor := &interceptors.VkInterceptor{
		Secret32bit: []byte(config.Secret32Bit),
		UserRepo:    userRepo,
		VkAuthRepo:  vkAuthRepo,
	}
	commandInterceptor := interceptors.NewCommandInterceptor(googleInterceptor, vkInterceptor)

	return &api.Server{
		GoogleOAuthConfig:  gC,
		VkOauthConfig:      vC,
		Interceptor:        commandInterceptor,
		UserRepo:           userRepo,
		GoogleAuthRepo:     gAuthRepo,
		VkAuthRepo:         vkAuthRepo,
		JWTAuthRepo:        simpleAuthRepo,
		BuffEntityRepo:     buffEntityRepo,
		BuffRepo:           buffRepo,
		ChakraRepo:         chakraRepo,
		ProfileUseCase:     profileUsecase,
		PasswordChangeRepo: passwordChangeRepo,
		Secret32bit:        []byte(config.Secret32Bit),
		JWTAuth:            jwtauth.New("HS256", []byte(config.JWTSecret), nil),
		JWTExpiry:          exp,
	}
}
