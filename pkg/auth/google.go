package auth

import (
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type GoogleConfig struct {
	Config      *oauth2.Config
	StateName   string
	ResourceURL string
	RefreshURL  string // https://oauth2.googleapis.com/token POST
	Client      *http.Client
}

type Data struct {
	GoogleID string `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
}

func (g *GoogleConfig) GetData(token string) (Data, error) {
	response, err := http.Get(g.ResourceURL + token)
	if err != nil {
		return Data{}, err
	}
	defer response.Body.Close()
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Data{}, err
	}

	var resp Data
	if err := json.Unmarshal(data, &resp); err != nil {
		return Data{}, err
	}
	return resp, nil
}

func (g *GoogleConfig) RefreshAccessToken(token string) ([]byte, error) {
	urlEncBody := url.Values{}
	urlEncBody.Set("client_id", g.Config.ClientID)
	urlEncBody.Set("client_secret", g.Config.ClientSecret)
	urlEncBody.Set("grant_type", "refresh_token")
	urlEncBody.Set("refresh_token", token)

	r, err := http.NewRequest("POST", g.RefreshURL, strings.NewReader(urlEncBody.Encode()))
	if err != nil {
		return nil, err
	}
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(urlEncBody.Encode())))

	resp, err := g.Client.Do(r)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("non-200 code is returned, message - %s", string(data))
	}
	var tokenResp struct {
		AccessToken string `json:"access_token"`
	}
	err = json.Unmarshal(data, &tokenResp)
	if err != nil {
		return nil, err
	}
	return []byte(tokenResp.AccessToken), nil
}
