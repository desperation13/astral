package auth

type Type uint

const (
	NOT_SET Type = iota
	JWT
	GOOGLE
	VK
)
