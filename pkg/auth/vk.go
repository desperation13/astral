package auth

import "golang.org/x/oauth2"

type VkConfig struct {
	Config      *oauth2.Config
	StateName   string
	ResourceURL string
	RefreshURL  string // https://oauth2.googleapis.com/token POST
}
