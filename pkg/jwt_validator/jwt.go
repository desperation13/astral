package jwt_validator

import (
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
)

var (
	ErrNotValidToken = errors.New("not valid token or not exist")
)

func ValidateToken(ctx context.Context) (*jwt.Token, jwt.MapClaims, error) {
	token, claims, err := jwtauth.FromContext(ctx)
	if err != nil {
		return nil, nil, err
	}
	if token == nil || !token.Valid {
		return nil, nil, ErrNotValidToken
	}
	return token, claims, nil
}
