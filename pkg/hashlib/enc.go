package hashlib

import "encoding/base64"

func EncryptAndEncode(data string, secret []byte) (string, error) {
	encData, err := Encrypt([]byte(data), secret)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(encData), nil
}
