package hashlib

import (
	"encoding/base64"
	"golang.org/x/crypto/scrypt"
)

func HashAndSalt(pwd []byte, salt []byte) string {
	hash, _ := scrypt.Key(pwd, salt, 16384, 8, 1, 32)
	return base64.URLEncoding.EncodeToString(hash)
}
