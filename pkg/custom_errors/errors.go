package custom_errors

type CustomError struct {
	Message string            `json:"message"`
	Code    int               `json:"code"`
	Errors  map[string]string `json:"errors"`
}

var (
	ServerError = CustomError{
		Message: "Internal server error",
		Code:    1,
	}
	NotFoundUser = CustomError{
		Message: "User was not found ",
		Code:    2,
	}
	TokenIsNotValid = CustomError{
		Message: "Given token is not valid",
		Code:    3,
	}
	InvalidArguments = CustomError{
		Message: "Some of the fields are invalid",
		Code:    4,
	}
	InvalidJSON = CustomError{
		Message: "JSON is not valid",
		Code:    5,
	}
	InvalidSignInCredentials = CustomError{
		Message: "invalid sign-in credentials",
		Code:    6,
	}
	UnauthorizedError = CustomError{
		Message: "Unauthorized",
		Code:    7,
	}
	CodeHasExpired = CustomError{
		Message: "recovery code has expired, try again",
		Code:    8,
	}
	InvalidPassword = CustomError{
		Message: "invalid password",
		Code:    9,
	}
	NoSuchBuffEntity = CustomError{
		Message: "no such buff entity",
		Code:    10,
	}
)
