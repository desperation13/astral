package common_response

type Response struct {
	Data Data `json:"data"`
}

type Data struct {
	Message string `json:"message"`
}

var (
	OK = Response{
		Data: Data{
			Message: "OK",
		},
	}
)
