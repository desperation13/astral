package chakras

import "github.com/jinzhu/gorm"

type Chakra struct {
	gorm.Model

	Name string `gorm:"unique"`
}
