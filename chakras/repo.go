package chakras

import "github.com/jinzhu/gorm"

type Repo struct {
	db *gorm.DB
}

func New(db *gorm.DB) *Repo {
	return &Repo{db: db}
}

func (r *Repo) GetByID(id uint) (*Chakra, error) {
	var c Chakra
	err := r.db.Take(&c, "id = ?", id).Error
	return &c, err
}
