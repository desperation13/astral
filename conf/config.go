package conf

import "github.com/caarlos0/env/v6"

type Config struct {
	Postgres    string `env:"POSTGRES" envDefault:"host=db port=5432 dbname=postgres user=postgres password=9xYjNoIP sslmode=disable"`
	GoogleOAuth GoogleOAuth
	VKOAuth     VKOAuth
	Secret32Bit string `env:"SECRET_32_BIT"`
	JWTSecret   string `env:"JWT_SECRET"`
	JWTExpiry   string `env:"JWT_EXPIRY" envDefault:"1"`
	Port        string `env:"PORT" envDefault:":8080"`
}

type GoogleOAuth struct {
	RedirectURL  string `env:"GOOGLE_REDIRECT_URL" envDefault:"http://astrality.live/api/google/oauth/callback"`
	ClientID     string `env:"GOOGLE_CLIENT_ID"`
	ClientSecret string `env:"GOOGLE_CLIENT_SECRET"`
}

type VKOAuth struct {
	RedirectURL  string `env:"VK_REDIRECT_URL" envDefault:"http://astrality.live/vk/oauth/callback"`
	ClientID     string `env:"VK_CLIENT_ID"`
	ClientSecret string `env:"VK_CLIENT_SECRET"`
}

func ReadConfig() (*Config, error) {
	var config Config
	err := env.Parse(&config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
