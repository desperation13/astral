package api

import (
	"bitbucket.org/desperation13/astral/pkg/logging"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"
	"github.com/sirupsen/logrus"
)

func createRouter(s *Server) chi.Router {
	logger := logrus.New()
	logger.Formatter = &logrus.JSONFormatter{
		DisableTimestamp: true,
	}

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Recoverer)
	r.Use(logging.NewStructuredLogger(logger))

	r.Group(func(r chi.Router) {
		r.Route("/api", func(r chi.Router) {
			r.Use(middleware.SetHeader("Content-Type", "application/json"))
			r.Group(func(r chi.Router) {
				r.Get("/google/oauth/login", s.oauthGoogleLogin)
				r.Get("/google/oauth/callback", s.oauthGoogleCallback)
				r.Get("/vk/oauth/login", s.oauthVkLogin)
				r.Get("/vk/oauth/callback", s.oauthVkCallback)
				r.Post("/sign_up", s.jwtSignUp)
				r.Post("/refresh", s.jwtRefresh)

				r.Post("/forgot_password", s.forgotPassword)
				r.Get("/forgot_password_callback", s.forgotPasswordCallback)
				r.Post("/reset_password", s.resetPassword)
			})
			r.Group(func(r chi.Router) {
				r.Use(jwtauth.Verifier(s.JWTAuth))
				r.Use(s.auth)
				r.Get("/hello", s.Hello)
				r.Get("/buffs", s.getUserBuffs)
				r.Post("/buy_buff", s.buyBuff)
			})
		})
	})
	return r
}
