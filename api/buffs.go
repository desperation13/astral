package api

import (
	"bitbucket.org/desperation13/astral/buffs"
	"bitbucket.org/desperation13/astral/pkg/custom_errors"
	"bitbucket.org/desperation13/astral/profile"
	"bitbucket.org/desperation13/astral/users"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/http"
	"time"
)

func (s *Server) buyBuff(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(*users.User)
	var req struct {
		Name string `json:"name"`
	}
	if err := s.fromJSON(r, &req); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	be, err := s.BuffEntityRepo.GetByName(req.Name)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			s.renderError(fmt.Errorf("no record found with such name = %s", req.Name), w, r, custom_errors.NoSuchBuffEntity, http.StatusBadRequest)
			return
		}
		s.renderServerError(err, w, r)
		return
	}
	chakra, err := s.ChakraRepo.GetByID(be.ChakraID)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	now := time.Now().UTC()
	endsAt := now.Add(time.Hour * time.Duration(be.ExpirationTimeHours))
	buff := &buffs.Buff{
		EndsAt:       endsAt,
		BuffEntityID: be.ID,
		UserID:       user.ID,
	}
	if err := s.BuffRepo.Save(buff); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	type retBuff struct {
		EndsAt     time.Time `json:"ends_at"`
		BuffName   string    `json:"buff_name"`
		ChakraName string    `json:"chakra_name"`
		BuffImgURL string    `json:"buff_img_url"`
		BuffText   string    `json:"buff_text"`
	}
	s.render(w, r, retBuff{
		EndsAt:     buff.EndsAt,
		BuffName:   be.Name,
		ChakraName: chakra.Name,
		BuffImgURL: be.ImgURL,
		BuffText:   be.Text,
	}, http.StatusOK)
}

func (s *Server) getUserBuffs(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(*users.User)
	buffInfos, err := s.ProfileUseCase.GetUserBuffInfos(user.ID)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}

	type retBuff struct {
		EndsAt     time.Time `json:"ends_at"`
		BuffName   string    `json:"buff_name"`
		ChakraName string    `json:"chakra_name"`
		BuffImgURL string    `json:"buff_img_url"`
		BuffText   string    `json:"buff_text"`
	}
	conv := func(info profile.BuffInfo) retBuff {
		return retBuff{
			EndsAt:     info.Buff.EndsAt,
			BuffName:   info.BuffEntity.Name,
			ChakraName: info.Chakra.Name,
			BuffImgURL: info.BuffEntity.ImgURL,
			BuffText:   info.BuffEntity.Text,
		}
	}

	var res []retBuff
	for _, i := range buffInfos {
		res = append(res, conv(i))
	}
	s.render(w, r, res, http.StatusOK)
}
