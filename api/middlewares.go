package api

import (
	"bitbucket.org/desperation13/astral/pkg/jwt_validator"
	"context"
	"github.com/go-chi/jwtauth"
	"net/http"
	"strconv"
)

func (s *Server) auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, claims, err := jwt_validator.ValidateToken(r.Context())

		if err != nil {
			if err == jwtauth.ErrNoTokenFound {
				data, err := s.Interceptor.Do(r)
				if err != nil {
					s.renderUnauthorizedErr(err, w, r)
					return
				}
				if data.Cookie != nil {
					http.SetCookie(w, data.Cookie)
				}
				ctx := context.WithValue(r.Context(), "user", data.User)
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}
			s.renderUnauthorizedErr(err, w, r)
			return
		}

		// Token is authenticated, pass it through
		userIDStr := claims["sub"].(string)
		userID, _ := strconv.Atoi(userIDStr)
		user, _ := s.UserRepo.GetByID(uint(userID))
		ctx := context.WithValue(r.Context(), "user", user)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
