package api

import (
	"bitbucket.org/desperation13/astral/pkg/custom_errors"
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"bitbucket.org/desperation13/astral/users"
	"bitbucket.org/desperation13/astral/users/jwt_info"
	"fmt"
	"github.com/goware/emailx"
	"net/http"
	"time"
)

type SignUpInfo struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func validate(s SignUpInfo) (string, error) {
	minSizeErr := fmt.Errorf("minimum size should be 6")
	if len(s.Password) < 6 {
		return "password", minSizeErr
	}
	if len(s.Username) < 6 {
		return "username", minSizeErr
	}
	if len(s.Email) < 6 {
		return "email", minSizeErr
	}
	err := emailx.Validate(s.Email)
	if err != nil {
		if err == emailx.ErrInvalidFormat {
			return "email", fmt.Errorf("wrong format")
		}

		if err == emailx.ErrUnresolvableHost {
			return "email", fmt.Errorf("unresolvable host")
		}
		return "email", fmt.Errorf("invalid email")
	}
	return "", nil
}

func (s *Server) jwtSignUp(w http.ResponseWriter, r *http.Request) {
	var req SignUpInfo
	if err := s.fromJSON(r, &req); err != nil {
		s.renderError(err, w, r, custom_errors.InvalidJSON, http.StatusBadRequest)
		return
	}
	if kind, err := validate(req); err != nil {
		custErr := custom_errors.InvalidSignInCredentials
		custErr.Errors = map[string]string{
			kind: err.Error(),
		}
		s.renderError(err, w, r, custErr, http.StatusBadRequest)
		return
	}
	var (
		info *jwt_info.JWTAuthInfo
		err  error
	)
	info, err = s.JWTAuthRepo.GetInfoByFilter(jwt_info.HashedPasswordFilter{HashedPassword: hashlib.HashAndSalt([]byte(req.Password), s.Secret32bit)})
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if info == nil {
		u := &users.User{
			Username: req.Username,
		}
		err := s.UserRepo.Save(u)
		if err != nil {
			s.renderServerError(err, w, r)
			return
		}
		info = &jwt_info.JWTAuthInfo{
			HashedPassword: hashlib.HashAndSalt([]byte(req.Password), s.Secret32bit),
			Email:          req.Email,
			UserID:         u.ID,
		}
		err = s.JWTAuthRepo.Save(info)
		if err != nil {
			t := time.Now()
			u.DeletedAt = &t
			s.UserRepo.Save(u)
			s.renderServerError(err, w, r)
			return
		}
	} else {
		user, err := s.UserRepo.GetByID(info.UserID)
		if err != nil {
			s.renderServerError(err, w, r)
			return
		}
		if info.Email != req.Email || user.Username != req.Username {
			err := fmt.Errorf("user has written password with different email || username, email = %s, username = %s", req.Email, req.Username)
			custErr := custom_errors.InvalidSignInCredentials
			custErr.Errors = map[string]string{
				"password": "invalid password",
			}
			s.renderError(err, w, r, custErr, http.StatusBadRequest)
			return
		}
	}
	at, rt := s.generateJWTPair(info.UserID)
	s.render(w, r, map[string]string{"access_token": at, "refresh_token": rt}, http.StatusOK)
}
