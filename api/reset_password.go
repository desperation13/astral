package api

import (
	"bitbucket.org/desperation13/astral/pkg/custom_errors"
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"bitbucket.org/desperation13/astral/users/jwt_info"
	"fmt"
	"net/http"
	"time"
)

func (s *Server) resetPassword(w http.ResponseWriter, r *http.Request) {
	var req struct {
		NewPassword string `json:"new_password"`
	}
	if err := s.fromJSON(r, &req); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if len(req.NewPassword) < 6 {
		s.renderError(fmt.Errorf("length of password is less than 6"), w, r, custom_errors.InvalidPassword, http.StatusBadRequest)
		return
	}

	UUID := r.URL.Query().Get("code")

	p, err := s.PasswordChangeRepo.GetByHashedID(hashlib.HashAndSalt([]byte(UUID), s.Secret32bit))
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if p == nil {
		s.renderUnauthorizedErr(fmt.Errorf("no record found with uuid = %s", UUID), w, r)
		return
	}

	timeToLive := p.CreatedAt.UTC().Add(time.Hour * 24)
	now := time.Now().UTC()
	if now.After(timeToLive) {
		s.renderError(fmt.Errorf("code has expired"), w, r, custom_errors.CodeHasExpired, http.StatusBadRequest)
		return
	}
	info, err := s.JWTAuthRepo.GetInfoByFilter(jwt_info.UserIDFilter{UserID: p.UserID})
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}

	info.HashedPassword = hashlib.HashAndSalt([]byte(req.NewPassword), s.Secret32bit)
	if err := s.JWTAuthRepo.Save(info); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	s.render(w, r, map[string]string{"message": "OK"}, http.StatusOK)
	return
}
