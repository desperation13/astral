package api

import (
	"bitbucket.org/desperation13/astral/pkg/custom_errors"
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"bitbucket.org/desperation13/astral/users/jwt_info"
	"bitbucket.org/desperation13/astral/users/password_changes"
	"bytes"
	"fmt"
	"github.com/google/uuid"
	"net/http"
	"net/smtp"
	"time"
)

func sendEmail(addr, sender, recipient, payload string) error {
	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	// Set the sender and recipient.
	c.Mail(sender)
	c.Rcpt(recipient)
	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		return err
	}
	defer wc.Close()
	buf := bytes.NewBufferString(payload)
	if _, err = buf.WriteTo(wc); err != nil {
		return err
	}
	return nil
}

func (s *Server) forgotPassword(w http.ResponseWriter, r *http.Request) {
	var req struct {
		Email string `json:"email"`
	}
	if err := s.fromJSON(r, &req); err != nil {
		s.renderServerError(err, w, r)
		return
	}

	u, err := s.JWTAuthRepo.GetInfoByFilter(jwt_info.EmailFilter{Email: req.Email})
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if u == nil {
		s.renderError(fmt.Errorf("not found user with email = %s", req.Email), w, r, custom_errors.NotFoundUser, http.StatusBadRequest)
		return
	}

	uid := uuid.New().String()
	hashedID := hashlib.HashAndSalt([]byte(uid), s.Secret32bit)
	p := password_changes.PasswordChange{
		HashedID: hashedID,
		UserID:   u.UserID,
	}
	if err := s.PasswordChangeRepo.Save(&p); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	// TODO: send uuid to email
	s.render(w, r, map[string]string{"uuid": uid}, http.StatusOK)
}

func (s *Server) forgotPasswordCallback(w http.ResponseWriter, r *http.Request) {
	UUID := r.URL.Query().Get("code")

	p, err := s.PasswordChangeRepo.GetByHashedID(hashlib.HashAndSalt([]byte(UUID), s.Secret32bit))
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if p == nil {
		s.renderUnauthorizedErr(fmt.Errorf("no record found with uuid = %s", UUID), w, r)
		return
	}

	timeToLive := p.CreatedAt.UTC().Add(time.Hour * 24)
	now := time.Now().UTC()
	if now.After(timeToLive) {
		s.renderError(fmt.Errorf("code has expired"), w, r, custom_errors.CodeHasExpired, http.StatusBadRequest)
		return
	}
	s.render(w, r, map[string]string{"message": "you can now enter new password"}, http.StatusOK)
	return
}
