package api

import (
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"bitbucket.org/desperation13/astral/users"
	"bitbucket.org/desperation13/astral/users/google_info"
	"context"
	"encoding/base64"
	"fmt"
	"golang.org/x/oauth2"
	"math/rand"
	"net/http"
	"time"
)

func (s *Server) oauthGoogleLogin(w http.ResponseWriter, r *http.Request) {
	expiration := time.Now().Add(time.Hour)

	b := make([]byte, 16)
	rand.Read(b)

	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: s.GoogleOAuthConfig.StateName, Value: state, Expires: expiration}

	// This options needed to get refresh token from google callback
	offlineOption := oauth2.SetAuthURLParam("access_type", "offline")
	promptOption := oauth2.SetAuthURLParam("prompt", "consent")

	redirectURI := s.GoogleOAuthConfig.Config.AuthCodeURL(cookie.Value, offlineOption, promptOption)
	s.LogField(r, "redirect_url", redirectURI)
	http.SetCookie(w, &cookie)
	h := w.Header()
	// That was for NGINX. NGINX will resolve inner endpoint '/google/login' and proxy pass user further to google login page (redirect_uri)
	h.Set("X-Accel-Redirect", "/google/login")
	h.Set("redirect", redirectURI)
	http.Redirect(w, r, redirectURI, http.StatusTemporaryRedirect)
}

const (
	GOOGLE_ENC_TOKEN = "gstate"
	VK_ENC_TOKEN     = "vkstate"
)

func (s *Server) oauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	// Read oauthState from Cookie
	oauthState, _ := r.Cookie(s.GoogleOAuthConfig.StateName)

	if r.FormValue("state") != oauthState.Value {
		s.LogField(r, "error", "invalid oauth google state")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	token, err := s.GoogleOAuthConfig.Config.Exchange(context.Background(), r.FormValue("code"))
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	s.LogField(r, "google_token", token)

	data, err := s.GoogleOAuthConfig.GetData(token.AccessToken)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}

	hT := hashlib.HashAndSalt([]byte(token.AccessToken), s.Secret32bit)

	gInfo, err := s.GoogleAuthRepo.GetInfoByGoogleServiceID(data.GoogleID)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if gInfo == nil {
		// create user and his gauth_info
		u := users.User{
			Username: data.Email,
		}
		err := s.UserRepo.Save(&u)
		if err != nil {
			s.renderServerError(err, w, r)
			return
		}
		g := google_info.GoogleAuthInfo{
			ServiceID:    data.GoogleID,
			HashedToken:  hT,
			RefreshToken: token.RefreshToken,
			UserID:       u.ID,
		}
		if err := s.GoogleAuthRepo.Save(&g); err != nil {
			t := time.Now()
			u.DeletedAt = &t
			s.UserRepo.Save(&u)
			s.renderServerError(err, w, r)
			return
		}
	} else {
		// update gauth_info
		gInfo.HashedToken = hT
		gInfo.RefreshToken = token.RefreshToken
		if err := s.GoogleAuthRepo.Save(gInfo); err != nil {
			s.renderServerError(err, w, r)
			return
		}
	}

	encToken, err := hashlib.EncryptAndEncode(token.AccessToken, s.Secret32bit)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:    GOOGLE_ENC_TOKEN,
		Value:   encToken,
		Expires: time.Now().Add(time.Hour * 24 * 30 * 6),
		Path:    "/api",
	})

	fmt.Fprintf(w, "UserInfo: %s\n", data.GoogleID)
}
