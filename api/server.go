package api

import (
	"bitbucket.org/desperation13/astral/buffs"
	"bitbucket.org/desperation13/astral/chakras"
	"bitbucket.org/desperation13/astral/interceptors"
	"bitbucket.org/desperation13/astral/pkg/auth"
	"bitbucket.org/desperation13/astral/pkg/common_response"
	"bitbucket.org/desperation13/astral/pkg/custom_errors"
	"bitbucket.org/desperation13/astral/profile"
	"bitbucket.org/desperation13/astral/users"
	"bitbucket.org/desperation13/astral/users/google_info"
	"bitbucket.org/desperation13/astral/users/jwt_info"
	"bitbucket.org/desperation13/astral/users/password_changes"
	"bitbucket.org/desperation13/astral/users/vk_info"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"io/ioutil"
	"net/http"
	"time"
)

type userRepo interface {
	Save(u *users.User) error
	GetByID(id uint) (*users.User, error)
	GetByEmail(email string) (*users.User, error)
	GetByServiceID(serviceID string, serviceName auth.Type) (*users.User, error)
}

type googleAuthRepo interface {
	Save(u *google_info.GoogleAuthInfo) error
	GetInfoByGoogleServiceID(serviceID string) (*google_info.GoogleAuthInfo, error)
	GetInfoByHashedToken(hT string) (*google_info.GoogleAuthInfo, error)
}

type vkAuthRepo interface {
	Save(u *vk_info.VkAuthInfo) error
	GetInfoByVkServiceID(serviceID string) (*vk_info.VkAuthInfo, error)
	GetInfoByHashedToken(hT string) (*vk_info.VkAuthInfo, error)
}

type jwtAuthRepo interface {
	Save(i *jwt_info.JWTAuthInfo) error
	GetInfoByFilter(f jwt_info.Filter) (*jwt_info.JWTAuthInfo, error)
}

type buffEntityRepo interface {
	GetByName(name string) (*buffs.BuffEntity, error)
}

type buffRepo interface {
	Save(b *buffs.Buff) error
}

type chakraRepo interface {
	GetByID(id uint) (*chakras.Chakra, error)
}

type passwordChangeRepo interface {
	Save(p *password_changes.PasswordChange) error
	GetByHashedID(hashedID string) (*password_changes.PasswordChange, error)
}

type profileUseCase interface {
	GetUserBuffInfos(userID uint) ([]profile.BuffInfo, error)
}

type Server struct {
	GoogleOAuthConfig  *auth.GoogleConfig
	VkOauthConfig      *auth.VkConfig
	UserRepo           userRepo
	GoogleAuthRepo     googleAuthRepo
	VkAuthRepo         vkAuthRepo
	JWTAuthRepo        jwtAuthRepo
	BuffEntityRepo     buffEntityRepo
	BuffRepo           buffRepo
	ChakraRepo         chakraRepo
	ProfileUseCase     profileUseCase
	Interceptor        *interceptors.CommandInterceptor
	PasswordChangeRepo passwordChangeRepo
	Host               string
	Secret32bit        []byte
	JWTAuth            *jwtauth.JWTAuth
	JWTSecret          string
	JWTExpiry          int
}

func (s *Server) render(w http.ResponseWriter, r *http.Request, data interface{}, status int) {
	w.WriteHeader(status)

	var response interface{}
	if data != nil {
		response = data
	} else {
		response = common_response.OK
	}

	s.LogField(r, "response", response)

	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		s.LogField(r, "err", err)
	}
}

func (s *Server) renderError(backendErr error, w http.ResponseWriter, r *http.Request, customErr custom_errors.CustomError, status int) {
	s.LogField(r, "serverError", backendErr)
	s.render(w, r, customErr, status)
}

func (s *Server) renderUnauthorizedErr(backendErr error, w http.ResponseWriter, r *http.Request) {
	s.LogField(r, "serverError", backendErr)
	s.render(w, r, custom_errors.UnauthorizedError, http.StatusBadRequest)
}

func (s *Server) renderServerError(err error, w http.ResponseWriter, r *http.Request) {
	s.LogField(r, "serverError", err)
	s.render(w, r, custom_errors.ServerError, http.StatusBadRequest)
}

func (s *Server) getIDByAccessToken(token string) (string, error) {
	response, err := http.Get(s.GoogleOAuthConfig.ResourceURL + token)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	fmt.Println("data", string(data))

	type Data struct {
		ID    string `json:"id"`
		Email string `json:"email"`
	}
	var resp Data
	if err := json.Unmarshal(data, &resp); err != nil {
		return "", err
	}
	return resp.ID, nil
}

func (s *Server) fromJSON(r *http.Request, res interface{}) error {
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(res)
}

func (s *Server) generateJWTPair(userID uint) (string, string) {
	cur := time.Now().UTC()
	exp := cur.Add(time.Minute * time.Duration(s.JWTExpiry))
	_, aT, _ := s.JWTAuth.Encode(jwt.StandardClaims{
		ExpiresAt: exp.Unix(),
		Subject:   fmt.Sprintf("%d", userID),
	})
	_, rT, _ := s.JWTAuth.Encode(jwt.StandardClaims{
		ExpiresAt: cur.Add(time.Hour * 24 * 60).Unix(),
		Subject:   fmt.Sprintf("%d", userID),
	})
	return aT, rT
}
