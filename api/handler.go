package api

import (
	"github.com/go-chi/chi"
	"net/http"
)

type Handler struct {
	serv   *Server
	router chi.Router
}

func NewHandler(s *Server) *Handler {
	router := createRouter(s)

	return &Handler{
		serv:   s,
		router: router,
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.router.ServeHTTP(w, r)
}
