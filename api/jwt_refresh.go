package api

import (
	"bitbucket.org/desperation13/astral/users/jwt_info"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"reflect"
	"strconv"
)

func (s *Server) jwtRefresh(w http.ResponseWriter, r *http.Request) {
	var tokenReq struct {
		RefreshToken string `json:"refresh_token"`
	}
	if err := s.fromJSON(r, &tokenReq); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	t, err := s.JWTAuth.Decode(tokenReq.RefreshToken)
	if err != nil {
		s.renderUnauthorizedErr(err, w, r)
		return
	}
	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		s.renderUnauthorizedErr(fmt.Errorf("can't get map claims"), w, r)
		return
	}
	userIDStr, ok := claims["sub"].(string)
	if !ok {
		s.renderUnauthorizedErr(fmt.Errorf("failed to convert user_id to string %+v, type = %v", claims["sub"], reflect.TypeOf(claims["sub"])), w, r)
		return
	}

	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		s.renderUnauthorizedErr(fmt.Errorf("failed to conver user_id (string) to int"), w, r)
		return
	}

	info, err := s.JWTAuthRepo.GetInfoByFilter(jwt_info.UserIDFilter{UserID: uint(userID)})
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if info == nil {
		s.renderUnauthorizedErr(fmt.Errorf("no user found with id = %d", uint(userID)), w, r)
		return
	}
	at, rt := s.generateJWTPair(info.UserID)
	s.render(w, r, map[string]string{"access_token": at, "refresh_token": rt}, http.StatusOK)
}
