package api

import "net/http"

func (s *Server) Hello(w http.ResponseWriter, r *http.Request) {
	s.render(w, r, map[string]string{
		"hello": "hi",
	}, http.StatusOK)
}
