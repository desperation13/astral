package api

import (
	"bitbucket.org/desperation13/astral/pkg/hashlib"
	"bitbucket.org/desperation13/astral/users"
	"bitbucket.org/desperation13/astral/users/vk_info"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"
)

func (s *Server) oauthVkLogin(w http.ResponseWriter, r *http.Request) {
	expiration := time.Now().Add(time.Hour)

	b := make([]byte, 16)
	rand.Read(b)

	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: s.VkOauthConfig.StateName, Value: state, Expires: expiration}

	redirectURL := s.VkOauthConfig.Config.AuthCodeURL(cookie.Value)

	http.SetCookie(w, &cookie)
	http.Redirect(w, r, redirectURL, http.StatusTemporaryRedirect)
}

func (s *Server) oauthVkCallback(w http.ResponseWriter, r *http.Request) {
	// Read oauthState from Cookie
	oauthState, _ := r.Cookie(s.VkOauthConfig.StateName)

	if r.FormValue("state") != oauthState.Value {
		s.LogField(r, "error", "invalid oauth google state")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	config := s.VkOauthConfig.Config
	values := fmt.Sprintf("?client_id=%s&client_secret=%s&redirect_uri=%s&code=%s", config.ClientID, config.ClientSecret, config.RedirectURL, r.FormValue("code"))
	resp, err := http.Get(s.VkOauthConfig.ResourceURL + values)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	var t struct {
		AccessToken string `json:"access_token"`
		ServiceID   uint   `json:"user_id"`
		Email       string `json:"email"`
	}
	if err := json.Unmarshal(data, &t); err != nil {
		s.renderServerError(err, w, r)
		return
	}
	hT := hashlib.HashAndSalt([]byte(t.AccessToken), s.Secret32bit)
	serviceID := fmt.Sprintf("%d", t.ServiceID)

	i, err := s.VkAuthRepo.GetInfoByVkServiceID(serviceID)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	if i == nil {
		// create user and his vkauth_info
		u := users.User{
			Username: t.Email,
		}
		err := s.UserRepo.Save(&u)
		if err != nil {
			s.renderServerError(err, w, r)
			return
		}
		vkInfo := vk_info.VkAuthInfo{
			ServiceID:   serviceID,
			HashedToken: hT,
			UserID:      u.ID,
		}
		if err := s.VkAuthRepo.Save(&vkInfo); err != nil {
			t := time.Now()
			u.DeletedAt = &t
			s.UserRepo.Save(&u)
			s.renderServerError(err, w, r)
			return
		}
	} else {
		i.HashedToken = hT
		if err := s.VkAuthRepo.Save(i); err != nil {
			s.renderServerError(err, w, r)
			return
		}
	}

	encToken, err := hashlib.EncryptAndEncode(t.AccessToken, s.Secret32bit)
	if err != nil {
		s.renderServerError(err, w, r)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:    VK_ENC_TOKEN,
		Value:   encToken,
		Expires: time.Now().Add(time.Hour * 24 * 30 * 6),
		Path:    "/api",
	})

	w.WriteHeader(http.StatusOK)
}
