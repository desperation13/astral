package users

import (
	"bitbucket.org/desperation13/astral/pkg/auth"
	"fmt"
	"github.com/jinzhu/gorm"
)

type Repository struct {
	db *gorm.DB
}

func (r *Repository) Save(u *User) error {
	return r.db.Save(u).Error
}

func (r *Repository) GetByID(id uint) (*User, error) {
	var u User
	err := r.db.Where("id = ?", id).Take(&u).Error
	return &u, err
}

func (r *Repository) GetByEmail(email string) (*User, error) {
	var u User
	err := r.db.Where("email = ?", email).First(&u).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &u, err
}

func (r *Repository) GetByServiceID(serviceID string, serviceType auth.Type) (*User, error) {
	table := ""
	switch serviceType {
	case auth.GOOGLE:
		table = "google_auth_infos as i"
	case auth.VK:
		table = "vk_auth_infos as i"
	default:
		return nil, fmt.Errorf("serviceType is not valid %d", serviceType)
	}
	var uu []User
	joinQuery := fmt.Sprintf("inner join %s on u.id = i.user_id where service_id = $1", table)
	err := r.db.Table("users as u").Joins(joinQuery, serviceID).Select("u.*").Scan(&uu).Error
	if err != nil {
		return nil, err
	}
	if len(uu) == 0 {
		return nil, nil
	}
	if len(uu) > 1 {
		return nil, fmt.Errorf("multiple user records returned by service_id = %s", serviceID)
	}
	return &uu[0], nil
}

func NewRepo(db *gorm.DB) *Repository {
	return &Repository{db: db}
}
