package jwt_info

import "github.com/jinzhu/gorm"

type Filter interface {
	Apply(db *gorm.DB) *gorm.DB
}

type HashedPasswordFilter struct {
	HashedPassword string
}

func (f HashedPasswordFilter) Apply(db *gorm.DB) *gorm.DB {
	return db.Where("hashed_password = ?", f.HashedPassword)
}

type UserIDFilter struct {
	UserID uint
}

func (f UserIDFilter) Apply(db *gorm.DB) *gorm.DB {
	return db.Where("user_id = ?", f.UserID)
}

type EmailFilter struct {
	Email string
}

func (f EmailFilter) Apply(db *gorm.DB) *gorm.DB {
	return db.Where("email = ?", f.Email)
}
