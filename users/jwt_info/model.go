package jwt_info

import "github.com/jinzhu/gorm"

type JWTAuthInfo struct {
	gorm.Model

	HashedPassword string `gorm:"unique"`
	Email          string `gorm:"unique"`

	UserID uint `sql:"type:integer REFERENCES users(id);not null"`
}
