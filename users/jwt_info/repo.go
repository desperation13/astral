package jwt_info

import "github.com/jinzhu/gorm"

type JWTAuthRepo struct {
	db *gorm.DB
}

func NewSimpleAuthRepo(db *gorm.DB) *JWTAuthRepo {
	return &JWTAuthRepo{db: db}
}

func (r *JWTAuthRepo) Save(i *JWTAuthInfo) error {
	return r.db.Save(i).Error
}

func (r *JWTAuthRepo) GetInfoByFilter(f Filter) (*JWTAuthInfo, error) {
	var j JWTAuthInfo
	db := r.db
	db = f.Apply(db)
	err := db.First(&j).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &j, err
}
