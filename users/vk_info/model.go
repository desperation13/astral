package vk_info

import "github.com/jinzhu/gorm"

type VkAuthInfo struct {
	gorm.Model

	ServiceID   string `gorm:"unique"`
	HashedToken string `gorm:"unique"`

	UserID uint `sql:"type:integer REFERENCES users(id);not null"`
}
