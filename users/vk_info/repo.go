package vk_info

import "github.com/jinzhu/gorm"

type VkRepo struct {
	db *gorm.DB
}

func NewVkRepo(db *gorm.DB) *VkRepo {
	return &VkRepo{db: db}
}

func (v *VkRepo) Save(i *VkAuthInfo) error {
	return v.db.Save(&i).Error
}

func (v *VkRepo) GetInfoByVkServiceID(serviceID string) (*VkAuthInfo, error) {
	var ret VkAuthInfo
	err := v.db.Take(&ret, "service_id = ?", serviceID).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &ret, err
}

func (v *VkRepo) GetInfoByHashedToken(hT string) (*VkAuthInfo, error) {
	var i VkAuthInfo
	err := v.db.Take(&i, "hashed_token = ?", hT).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &i, err
}
