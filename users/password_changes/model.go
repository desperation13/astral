package password_changes

import "github.com/jinzhu/gorm"

type PasswordChange struct {
	gorm.Model

	HashedID string `gorm:"unique"`
	UserID   uint   `sql:"type:integer REFERENCES users(id);not null"`
}
