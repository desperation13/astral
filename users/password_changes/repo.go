package password_changes

import "github.com/jinzhu/gorm"

type Repository struct {
	db *gorm.DB
}

func NewRepo(db *gorm.DB) *Repository {
	return &Repository{db: db}
}

func (r *Repository) Save(p *PasswordChange) error {
	return r.db.Save(p).Error
}

func (r *Repository) GetByHashedID(hashedID string) (*PasswordChange, error) {
	var p PasswordChange
	err := r.db.Take(&p, "hashed_id = ?", hashedID).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &p, err
}
