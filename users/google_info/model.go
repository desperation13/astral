package google_info

import "github.com/jinzhu/gorm"

type GoogleAuthInfo struct {
	gorm.Model

	ServiceID    string `gorm:"unique"`
	HashedToken  string `gorm:"unique"`
	RefreshToken string `gorm:"unique"`

	UserID uint `sql:"type:integer REFERENCES users(id);not null"`
}
