package google_info

import "github.com/jinzhu/gorm"

type GoogleRepo struct {
	db *gorm.DB
}

func NewGoogleRepo(db *gorm.DB) *GoogleRepo {
	return &GoogleRepo{db: db}
}

func (g *GoogleRepo) Save(i *GoogleAuthInfo) error {
	return g.db.Save(&i).Error
}

func (g *GoogleRepo) GetInfoByGoogleServiceID(serviceID string) (*GoogleAuthInfo, error) {
	var ret GoogleAuthInfo
	err := g.db.Take(&ret, "service_id = ?", serviceID).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &ret, err
}

func (g *GoogleRepo) GetInfoByHashedToken(hT string) (*GoogleAuthInfo, error) {
	var i GoogleAuthInfo
	err := g.db.Take(&i, "hashed_token = ?", hT).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	return &i, err
}
